/*
	This file is part of "Interface Fractures III - Silicon".
	Copyright (c) 2015 Luka Prinčič, All rights reserved.
	This program is free software distributed under
	GNU General Public Licence. See COPYING for more info.
	- - - - - - - - - - - - - - - - - - - - - - - - - - - -

  osc.pde - open sound control communication
  
                                                                              */

//_____________________________________________________________________________
void ctlin(int cc, int val) {  // midi control values, from Renoise (via SC)
  
  // debug:
  // println("### OSC: /ctlin cc:" + cc + " value:" + val);

  // patchbay

  if (cc == 3) { screenCleanHue = val; }
  if (cc == 4) { screenCleanSaturation = val; }
  if (cc == 5) { screenCleanBrightness = val; }
  if (cc == 6) { screenCleanAlpha = val; }
  if (cc == 7) { screenCleanFlickrAmount = val; }
  if (cc == 8) { screenCleanFlickrSpeed = val; }
  
  if (cc == 9) {
    if (val == 0) { drawSpectrumToggle = true;  }
    if (val == 1) { drawSpectrumToggle = false; }
    if (val == 2) {    drawTilesToggle = true;  }
    if (val == 3) {    drawTilesToggle = false; }
  }
  
  
  if (cc == 10) { drawSpectrumAFactor = val;  }
  if (cc == 11) { drawSpectrumThreshold = val;  }
  if (cc == 12) { drawSpectrumHeight = val;  }
  if (cc == 13) { drawSpectrumWidth = val;  }
  if (cc == 14) { drawSpectrumAwidth = val;  }
  if (cc == 15) { drawSpectrumSaturation = val;  }

  if (cc == 16) { tilesBgHue = val; }
  if (cc == 17) { tilesBgSat = val; }
  if (cc == 18) { tilesBgBri = val; }
  if (cc == 19) { tilesHue = val; }
  if (cc == 20) { tilesSat = val; }
  if (cc == 21) { tilesBri = val; }
  if (cc == 22) { tilesNumX = val; }
  if (cc == 23) { tilesNumY = val; }
  if (cc == 24) { tilesTexBank = val; }
  if (cc == 25) { tilesTexId = val; }
  if (cc == 26) { tilesTexSpeedX = val; }
  if (cc == 27) { tilesTexSpeedY = val; }
  if (cc == 28) { tilesOverlap = val; }

  //if (cc == 30) {}
 

  
}  


// ____________________________________________________________________________
void scosc(String mount, int val) {  // stuff coming directly from SuperCollider
  
  // debug:
  // println("### OSC: /sc mount:" + mount + " value:" + val);

  // patchbay
  if (mount.equals("testPatternToggle")) { testPatternToggle = boolean(val); println("~~~ testPatternToggle: " + val);}
  if (mount.equals("testPictureToggle")) { testPictureToggle = boolean(val); println("~~~ testPictureToggle: " + val);}
  if (mount.equals("drawSpectrumToggle")) { drawSpectrumToggle = boolean(val); println("~~~ drawSpectrumToggle: " + val);}
  if (mount.equals("drawTilesToggle")) { drawTilesToggle = boolean(val); println("~~~ drawTilesToggle: " + val);}

  
} // - - - - - - - - - - - - - - - - - - - - 


