#!/bin/bash
if [[ -z $1 ]]; then
	echo "error. no argument."
	echo "need the sketch path"
else

export DISPLAY=":0"
~/src/processing-2.2.1/processing-java \
    --force \
    --sketch=$1 \
    --run \
    --output=$1/output

fi
